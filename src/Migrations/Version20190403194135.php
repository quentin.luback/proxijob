<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190403194135 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE candidate ADD date_of_birth DATE NOT NULL, ADD address VARCHAR(255) NOT NULL, ADD postal_code VARCHAR(255) NOT NULL, ADD city VARCHAR(255) NOT NULL, ADD school VARCHAR(255) DEFAULT NULL, ADD diploma VARCHAR(255) DEFAULT NULL, ADD year_graduation INT DEFAULT NULL, ADD description LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE company ADD postal_code VARCHAR(255) NOT NULL, ADD city VARCHAR(255) NOT NULL, ADD year_creation INT NOT NULL, ADD inter_presence VARCHAR(255) NOT NULL, ADD general_description LONGTEXT NOT NULL, ADD values_description LONGTEXT DEFAULT NULL, ADD company_type VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE offer ADD description LONGTEXT NOT NULL, ADD exp_candidate VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE candidate DROP date_of_birth, DROP address, DROP postal_code, DROP city, DROP school, DROP diploma, DROP year_graduation, DROP description');
        $this->addSql('ALTER TABLE company DROP postal_code, DROP city, DROP year_creation, DROP inter_presence, DROP general_description, DROP values_description, DROP company_type');
        $this->addSql('ALTER TABLE offer DROP description, DROP exp_candidate');
    }
}
