<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompanyRepository")
 */
class Company
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $company_name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $company_type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $postal_code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="integer")
     */
    private $nb_employees;

    /**
     * @ORM\Column(type="integer")
     */
    private $year_creation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $inter_presence;

    /**
     * @ORM\Column(type="text")
     */
    private $general_description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $values_description;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="company", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offer", mappedBy="company", orphanRemoval=true)
     */
    private $offers;

    public function __construct()
    {
        $this->offers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompanyName(): ?string
    {
        return $this->company_name;
    }

    public function setCompanyName(string $company_name): self
    {
        $this->company_name = $company_name;

        return $this;
    }

    public function getCompanyType(): ?string
    {
        return $this->company_type;
    }

    public function setCompanyType(string $company_type): self
    {
        $this->company_type = $company_type;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postal_code;
    }

    public function setPostalCode(string $postal_code): self
    {
        $this->postal_code = $postal_code;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getNbEmployees(): ?int
    {
        return $this->nb_employees;
    }

    public function setNbEmployees(int $nb_employees): self
    {
        $this->nb_employees = $nb_employees;

        return $this;
    }

    public function getYearCreation(): ?int
    {
        return $this->year_creation;
    }

    public function setYearCreation(int $year_creation): self
    {
        $this->year_creation = $year_creation;

        return $this;
    }

    public function getInterPresence(): ?string
    {
        return $this->inter_presence;
    }

    public function setInterPresence(string $inter_presence): self
    {
        $this->inter_presence = $inter_presence;

        return $this;
    }

    public function getGeneralDescription(): ?string
    {
        return $this->general_description;
    }

    public function setGeneralDescription(string $general_description): self
    {
        $this->general_description = $general_description;

        return $this;
    }

    public function getValuesDescription(): ?string
    {
        return $this->values_description;
    }

    public function setValuesDescription(?string $values_description): self
    {
        $this->values_description = $values_description;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Offer[]
     */
    public function getOffers(): Collection
    {
        return $this->offers;
    }

    public function addOffer(Offer $offer): self
    {
        if (!$this->offers->contains($offer)) {
            $this->offers[] = $offer;
            $offer->setCompany($this);
        }

        return $this;
    }

    public function removeOffer(Offer $offer): self
    {
        if ($this->offers->contains($offer)) {
            $this->offers->removeElement($offer);
            // set the owning side to null (unless already changed)
            if ($offer->getCompany() === $this) {
                $offer->setCompany(null);
            }
        }

        return $this;
    }
}
