<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OfferRepository")
 */
class Offer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $contract;

    /**
     * @ORM\Column(type="date")
     */
    private $date_beginning;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $exp_candidate;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_publication;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="offers")
<<<<<<< HEAD
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id", nullable=false)
=======
     * @ORM\JoinColumn(nullable=false)
>>>>>>> de927cfdfd9b596bae26b0ac1537f570e7beebeb
     */
    private $company;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContract(): ?string
    {
        return $this->contract;
    }

    public function setContract(string $contract): self
    {
        $this->contract = $contract;

        return $this;
    }

    public function getDateBeginning(): ?\DateTimeInterface
    {
        return $this->date_beginning;
    }

    public function setDateBeginning(\DateTimeInterface $date_beginning): self
    {
        $this->date_beginning = $date_beginning;

        return $this;
    }

    public function getExpCandidate(): ?string
    {
        return $this->exp_candidate;
    }

    public function setExpCandidate(?string $exp_candidate): self
    {
        $this->exp_candidate = $exp_candidate;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
    
    public function getDatePublication(): ?\DateTimeInterface
    {
        return $this->date_publication;
    }

    public function setDatePublication(\DateTimeInterface $date_publication): self
    {
        $this->date_publication = $date_publication;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }
}
