<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use App\Entity\Offer;
use App\Entity\Company;
use App\Entity\Candidate;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }
    
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');
        // Création d'utilisateurs
        for($i = 0; $i < 10; $i++) {
            $user = new User();
            $user->setEmail($faker->email);
            $password = $this->encoder->encodePassword($user, '123');
            $user->setPassword($password);

            $manager->persist($user);

            /* Création d'un candidat ou d'une entreprise
               Si 0 = candidat, sinon entreprise */
            $type = rand(0, 1);
            $content = '<p>' . join($faker->paragraphs($nb = rand(2, 6)), '</p><p>') . '</p>';
            if($type == 0) {
                $candidate = new Candidate();
                $candidate->setFirstname($faker->firstName);
                $candidate->setLastname($faker->lastName);
                $candidate->setDateOfBirth($faker->dateTimeBetween($startDate = '-60 years', $endDate = '-16 years', $timezone = 'Europe/Paris'));
                $candidate->setAddress($faker->streetAddress);
                $candidate->setPostalCode($faker->postcode);
                $candidate->setCity($faker->city);
                $candidate->setPhone($faker->serviceNumber);
                $candidate->setSchool($faker->company);
                $candidate->setDiploma('Nom du diplôme');
                $candidate->setYearGraduation($faker->year);
                $candidate->setDescription($faker->text($maxNbChars = 1000));
                $candidate->setUser($user);
                
                $manager->persist($candidate);
            } else {
                $company = new Company();
                $company->setCompanyName($faker->company);
                $company->setCompanyType($faker->randomElement($array = array('Grande Entreprise','Start-up','PME','Association / Institution Publique / Laboratoire')));
                $company->setAddress($faker->streetAddress);
                $company->setPostalCode($faker->postcode);
                $company->setCity($faker->city);
                $company->setNbEmployees($faker->numberBetween($min = 1, $max = 100000));
                $company->setYearCreation($faker->year);
                $company->setInterPresence($faker->randomElement($array = array('yes','no')));
                $company->setGeneralDescription($content);
                $company->setValuesDescription($content);
                $company->setUser($user);

                $manager->persist($company);

                for($j = 0; $j < rand(2,5); $j++) {
                    $offer = new Offer();
                    $offer->setTitle($faker->jobTitle);
                    $offer->setContract($faker->randomElement($array = array('Stage','Alternance','CDD','CDI')));
                    $offer->setDateBeginning($faker->dateTimeBetween($startDate = 'now', $endDate = '+1 years', $timezone = 'Europe/Paris'));
                    $offer->setExpCandidate($faker->randomElement($array = array('Étudiant / Jeune diplômé','3 à 5 ans','6 à 10 ans','Plus de 10 ans')));
                    $offer->setDescription($content);
                    $offer->setDatePublication($faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now', $timezone = 'Europe/Paris'));
                    $offer->setCompany($company);

                    $manager->persist($offer);
                }
            }
        }
        $manager->flush();
    }
}
