<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Offer;
use App\Entity\Company;
use App\Form\OfferType;
use App\Entity\Candidate;
use App\Form\CompanyType;
use App\Form\CandidateType;
use App\Form\RegistrationType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/register", name="register")
     */
    public function register(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder)
    {
        $user = new User();

        $form = $this->createForm(RegistrationType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $hash = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hash);

            $manager->persist($user);
            $manager->flush();

            $this->addFlash('success', 'Vous êtes inscrit !');
            return $this->redirectToRoute('index');
        }

        return $this->render('security/register.html.twig', [
            'form' => $form->createView()
        ]);
    }
        
    /**
     * @Route("/login", name="login")
     */
    public function login() {
        
        return $this->render('security/login.html.twig');
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout() {
        $session->clear();
        return $this->render('security/login.html.twig');
    } 

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/candidate", name="candidate")
     */
    public function candidate(SessionInterface $session, Request $request, ObjectManager $manager)
    {
        $candidate = new Candidate();

        $form = $this->createForm(CandidateType::class, $candidate);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            $candidate->setUser($user);

            $manager->persist($candidate);
            $manager->flush();

            $this->addFlash('success', 'Vous êtes prêt en tant que candidat !');
            return $this->redirectToRoute('index');
        }

        return $this->render('security/candidate.html.twig', [
            'form' => $form->createView()
        ]);
    }  

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/company", name="company")
     */
    public function company(SessionInterface $session, Request $request, ObjectManager $manager)
    {
        $company = new Company();

        $form = $this->createForm(CompanyType::class, $company);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            $company->setUser($user);

            $manager->persist($company);
            $manager->flush();

            $this->addFlash('success', 'Vous êtes prêt en tant qu\'entreprise !');
            return $this->redirectToRoute('index');
        }

        return $this->render('security/company.html.twig', [
            'form' => $form->createView()
        ]);
    }  

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/offer", name="offer")
     * @Route("/offer/{id}", name="offer_edit")
     */
    public function offer(Offer $offer = null, SessionInterface $session, Request $request, ObjectManager $manager)
    {
        // Teste si l'offre existe
        if(!$offer) {
            $offer = new Offer();
        }
        
        $form = $this->createForm(OfferType::class, $offer);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $company = $this->getUser()->getCompany();
            dump($offer);
            dump($company);
            $offer->setCompany($company);

            if(!$offer->getId()) {
                $offer->setDatePublication(new \Datetime());
            }

            $manager->persist($offer);
            $manager->flush();

            $this->addFlash('success', 'Votre offre a bien été enregistrée');
            return $this->redirectToRoute('index');
        }

        return $this->render('security/offer.html.twig', [
            'form' => $form->createView()
        ]);
    }  
}
