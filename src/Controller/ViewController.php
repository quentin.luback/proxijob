<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Offer;
use App\Entity\Company;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ViewController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(UserInterface $user = null, SessionInterface $session, ObjectManager $manager)
    {
        // Teste si la personne vient de se connecter
        // Créer un paramètre email dans la session
        if($user && $session->has('email') == false) {
            $session->set('email', $user->getEmail());
        }
        $email = $session->get('email');

        // Récupère les dix offres les plus récentes
        $offers = $manager->getRepository(Offer::class)->findByMostRecent();
        // Récupére les entreprises liées aux offres
        foreach($offers as $offer) {
            $companyId = $offer->getCompany()->getId();
            $offerCompany = $manager->getRepository(Company::class)->findByCompany($companyId);
        }
        return $this->render('view/index.html.twig', [
            'session' => $session,
            'offers' => $offers,
            'date_now' => new \Datetime()
        ]);
    }
}
