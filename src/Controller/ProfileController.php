<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Offer;
use App\Entity\Company;
use App\Form\OfferType;
use App\Entity\Candidate;
use App\Form\CompanyType;
use App\Form\CandidateType;
use App\Form\RegistrationType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class ProfileController extends AbstractController
{
    /**
     * @Route("/profile", name="profile_index")
     */
    public function showProfile() {
        return $this->render('profile/index.html.twig');
    }

    /**
     * @Route("/profile/edit", name="profile_edit")
     */    
    public function editProfile(UserInterface $user = null, Candidate $candidate = null, Company $company = null, SessionInterface $session, Request $request, ObjectManager $manager)
    {
        if($user->getCandidate() != null) {
            $candidate = $user->getCandidate();
            $form = $this->createForm(CandidateType::class, $candidate);
        } else if($user->getCompany() != null) {
            $company = $user->getCompany();
            $form = $this->createForm(CompanyType::class, $company);
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if($candidate = $user->getCandidate()) {
                $manager->persist($candidate);
            } else if($company = $user->getCandidate()) {
                $manager->persist($company);
            }
            $manager->flush();

            $this->addFlash('success', 'Votre profil a bien été mis à jour');
            return $this->redirectToRoute('profile_index');
        }

        return $this->render('profile/profile.edit.html.twig', [
            'form' => $form->createView()
        ]);
    } 

    /**
     * @Route("/profile/offers-fav", name="profile_offers_fav")
     */
    public function showOffersFav() {
        return $this->render('profile/candidate/profile.fav.html.twig');
    }

    /**
     * @Route("/profile/appli-sent", name="profile_appli_sent")
     */
    public function showAppliSent() {
        return $this->render('profile/candidate/profile.sent.html.twig');
    }

    /**
     * @Route("/profile/offers-created", name="profile_offers_created")
     */
    public function showOffersCreated() {
        return $this->render('profile/company/profile.created.html.twig');
    }

    /**
     * @Route("/profile/appli-received", name="profile_appli_received")
     */
    public function showAppliReceived() {
        return $this->render('profile/company/profile.received.html.twig');
    }
}
