Plateforme de recrutement
Utilisateurs :
    Recruteurs
    Candidats

Interactions :
    Inscription
    Connexion
    Chercher offres
        Tout afficher (plus récent -> plus ancien)
        Premier filtre (CDI, stage, alternance)
        Plusieurs filtres (Localisation, Type-Métier, etc.)
        Mettre en favori
    Publier une offre
        Créer un descriptif de l'offre
            Nom du poste
            Nom de l'entreprise (cliquable pour afficher description de l'entreprise)
            Localisation (ville + pays)
            Date de publication
            Description générale
            Profil recherché
            Expérience demandée
            Rémunération
            Adresse
    Mettre à jour une offre
    Supprimer une offre
    Afficher offre en détail
        Descriptif de l'offre
        Pouvoir contacter par mail
        Envoyer CV + Lettre de motivation
        Mettre en favori
        Afficher le nombre de personnes ayant déjà postulé
    Gestion du compte
        Recruteurs :
            Descriptif de l'entreprise
            Afficher brièvement offres en cours de l'entreprise
        Candidats :
            Descriptif du Candidat
            Upload son CV
            Pouvoir contacter par mail
        Paramètres du compte
            Changement de coordonnées
            Suppression

Tables :
Login
Catégories
Entreprises
Candidats
Offres publiées
Offres en favori
Offres postulées

Liens :
    Nav : ProxiJob, Espace perso, Offres, Entreprises, Candidats, { S'inscrire, Se connecter / Icône Profil[Espace Perso, Mon Profil, Déconnexion] }
    Index : 
        Pas connecté : Formulaire de connexion et d'inscription
        Connecté : Espace Perso
    Espace Perso : Bandeau Profil[Image, Nom, Formation, Bouton "Voir mon profil", Offres en favori, Offres candidatées], Bandeau 10 offres les plus récentes
        Profil : Bandeau Profil[Image, Nom, Formation, Bouton "Modifier mon profil", Offres en favori, Offres candidatées], Bandeau Infos[Profil, Formation, Compte]
    
                   
    
