<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* security/profile.show.html.twig */
class __TwigTemplate_1416db9e9a1b0ef1748e3aab3a1a8d5e88a73be68878b7276a0f47e67f0d107b extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "security/profile.show.html.twig", 1);
        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/profile.show.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/profile.show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Profil";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<div class=\"container\">
    <div class=\"row\">
        ";
        // line 8
        $this->loadTemplate("block/profile.block.html.twig", "security/profile.show.html.twig", 8)->display($context);
        // line 9
        echo "        <div class=\"col-9\">
            <div class=\"container\">
                ";
        // line 11
        if ((twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "candidate", [], "any", true, true) &&  !(null === twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 11, $this->source); })()), "candidate", [])))) {
            // line 12
            echo "                <div class=\"row\">
                    <div class=\"col\">
                        <h1>Profil</h1>
                        <span class=\"d-block\">Prénom : ";
            // line 15
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 15, $this->source); })()), "candidate", []), "firstName", []), "html", null, true);
            echo "</span>
                        <span class=\"d-block\">Nom : ";
            // line 16
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 16, $this->source); })()), "candidate", []), "lastName", []), "html", null, true);
            echo "</span>
                        <p class=\"lead\">Description : ";
            // line 17
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 17, $this->source); })()), "candidate", []), "description", []), "html", null, true);
            echo "</p>
                        <span class=\"d-block\">Âge :</span>
                        <span class=\"d-block\">Adresse : ";
            // line 19
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 19, $this->source); })()), "candidate", []), "address", []), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 19, $this->source); })()), "company", []), "postalCode", []), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 19, $this->source); })()), "candidate", []), "city", []), "html", null, true);
            echo "</span>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col\">
                        <h3>Formation</h3>
                        <span class=\"d-block\">École/Université : ";
            // line 25
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 25, $this->source); })()), "candidate", []), "school", []), "html", null, true);
            echo "</span>
                        <span class=\"d-block\">Diplôme en cours : ";
            // line 26
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 26, $this->source); })()), "candidate", []), "diploma", []), "html", null, true);
            echo "</span>               
                        <span class=\"d-block\">Année d'obtention : ";
            // line 27
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 27, $this->source); })()), "candidate", []), "yearGraduation", []), "html", null, true);
            echo "</span>               
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col\">
                        <h3>Compte</h3>
                        <span class=\"d-block\">Adresse mail : ";
            // line 33
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 33, $this->source); })()), "email", []), "html", null, true);
            echo "</span>
                        <span class=\"d-block\">Téléphone : ";
            // line 34
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 34, $this->source); })()), "candidate", []), "phone", []), "html", null, true);
            echo "</span>
                    </div>
                </div>
                ";
        } elseif ((twig_get_attribute($this->env, $this->source,         // line 37
($context["user"] ?? null), "company", [], "any", true, true) &&  !(null === twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 37, $this->source); })()), "company", [])))) {
            // line 38
            echo "                <div class=\"row\">
                    <div class=\"col\">
                        <h1>Profil</h1>
                        <span class=\"d-block\">Nom : ";
            // line 41
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 41, $this->source); })()), "company", []), "companyName", []), "html", null, true);
            echo "</span>
                        <span class=\"d-block\">Adresse : ";
            // line 42
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 42, $this->source); })()), "company", []), "address", []), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 42, $this->source); })()), "company", []), "postalCode", []), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 42, $this->source); })()), "company", []), "city", []), "html", null, true);
            echo "</span>
                        <p class=\"lead\">Description : ";
            // line 43
            echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 43, $this->source); })()), "company", []), "generalDescription", []);
            echo "</p>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col\">
                        <h3>Caractéristiques</h3>
                        <span class=\"d-block\">Type : ";
            // line 49
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 49, $this->source); })()), "company", []), "companyType", []), "html", null, true);
            echo "</span>
                        <span class=\"d-block\">Nombre d'employés : ";
            // line 50
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 50, $this->source); })()), "company", []), "nbEmployees", []), "html", null, true);
            echo "</span>               
                        <span class=\"d-block\">Année de création : ";
            // line 51
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 51, $this->source); })()), "company", []), "yearCreation", []), "html", null, true);
            echo "</span>               
                        <span class=\"d-block\">Présence à l'international : ";
            // line 52
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 52, $this->source); })()), "company", []), "interPresence", []), "html", null, true);
            echo "</span>               
                        <p class=\"lead\">Valeurs / Culture : ";
            // line 53
            echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 53, $this->source); })()), "company", []), "valuesDescription", []);
            echo "</p>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col\">
                        <h3>Compte</h3>
                        <span class=\"d-block\">Adresse mail : ";
            // line 59
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 59, $this->source); })()), "email", []), "html", null, true);
            echo "</span>
                    </div>
                </div>
                ";
        }
        // line 63
        echo "            </div>
        </div>
    </div>
</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "security/profile.show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  214 => 63,  207 => 59,  198 => 53,  194 => 52,  190 => 51,  186 => 50,  182 => 49,  173 => 43,  165 => 42,  161 => 41,  156 => 38,  154 => 37,  148 => 34,  144 => 33,  135 => 27,  131 => 26,  127 => 25,  114 => 19,  109 => 17,  105 => 16,  101 => 15,  96 => 12,  94 => 11,  90 => 9,  88 => 8,  84 => 6,  75 => 5,  57 => 3,  27 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Profil{% endblock %}

{% block body %}
<div class=\"container\">
    <div class=\"row\">
        {% include 'block/profile.block.html.twig' %}
        <div class=\"col-9\">
            <div class=\"container\">
                {% if user.candidate is defined and user.candidate is not null %}
                <div class=\"row\">
                    <div class=\"col\">
                        <h1>Profil</h1>
                        <span class=\"d-block\">Prénom : {{ user.candidate.firstName }}</span>
                        <span class=\"d-block\">Nom : {{ user.candidate.lastName }}</span>
                        <p class=\"lead\">Description : {{ user.candidate.description }}</p>
                        <span class=\"d-block\">Âge :</span>
                        <span class=\"d-block\">Adresse : {{ user.candidate.address }} {{ user.company.postalCode }} {{ user.candidate.city }}</span>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col\">
                        <h3>Formation</h3>
                        <span class=\"d-block\">École/Université : {{ user.candidate.school }}</span>
                        <span class=\"d-block\">Diplôme en cours : {{ user.candidate.diploma }}</span>               
                        <span class=\"d-block\">Année d'obtention : {{ user.candidate.yearGraduation }}</span>               
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col\">
                        <h3>Compte</h3>
                        <span class=\"d-block\">Adresse mail : {{ user.email }}</span>
                        <span class=\"d-block\">Téléphone : {{ user.candidate.phone }}</span>
                    </div>
                </div>
                {% elseif user.company is defined and user.company is not null %}
                <div class=\"row\">
                    <div class=\"col\">
                        <h1>Profil</h1>
                        <span class=\"d-block\">Nom : {{ user.company.companyName }}</span>
                        <span class=\"d-block\">Adresse : {{ user.company.address }} {{ user.company.postalCode }} {{ user.company.city }}</span>
                        <p class=\"lead\">Description : {{ user.company.generalDescription | raw }}</p>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col\">
                        <h3>Caractéristiques</h3>
                        <span class=\"d-block\">Type : {{ user.company.companyType }}</span>
                        <span class=\"d-block\">Nombre d'employés : {{ user.company.nbEmployees }}</span>               
                        <span class=\"d-block\">Année de création : {{ user.company.yearCreation }}</span>               
                        <span class=\"d-block\">Présence à l'international : {{ user.company.interPresence }}</span>               
                        <p class=\"lead\">Valeurs / Culture : {{ user.company.valuesDescription | raw }}</p>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col\">
                        <h3>Compte</h3>
                        <span class=\"d-block\">Adresse mail : {{ user.email }}</span>
                    </div>
                </div>
                {% endif %}
            </div>
        </div>
    </div>
</div>

{% endblock %}", "security/profile.show.html.twig", "C:\\Symfony\\proxijob-project\\templates\\security\\profile.show.html.twig");
    }
}
