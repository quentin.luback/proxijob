<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* block/profile.block.html.twig */
class __TwigTemplate_120ba05aba3f9f7673848536071b96fd833ad70f5dc52149760f539baabe6801 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'profile_block' => [$this, 'block_profile_block'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "block/profile.block.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "block/profile.block.html.twig"));

        // line 1
        $this->displayBlock('profile_block', $context, $blocks);
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function block_profile_block($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "profile_block"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "profile_block"));

        // line 2
        echo "<div class=\"col-3\">
    <img src=\"https://via.placeholder.com/150\">
    ";
        // line 4
        if ((twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "candidate", [], "any", true, true) &&  !(null === twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 4, $this->source); })()), "candidate", [])))) {
            // line 5
            echo "        <span class=\"d-block\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 5, $this->source); })()), "candidate", []), "firstname", []), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 5, $this->source); })()), "candidate", []), "lastname", []), "html", null, true);
            echo "</span>
        <span class=\"d-block\">";
            // line 6
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 6, $this->source); })()), "candidate", []), "diploma", []), "html", null, true);
            echo "</span>
        ";
            // line 7
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 7, $this->source); })()), "request", []), "pathinfo", []) == "/")) {
                // line 8
                echo "        <a href=\"";
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profile_index");
                echo "\">Voir mon profil</a>
        ";
            } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 9
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 9, $this->source); })()), "request", []), "pathinfo", []) == "/profile")) {
                // line 10
                echo "        <a href=\"";
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profile_edit");
                echo "\">Modifier mon profil</a>
        ";
            }
            // line 12
            echo "        <div class=\"list-group\">
            <a href=\"#\" class=\"list-group-item list-group-item-action\">Mes offres favorites</a>
            <a href=\"#\" class=\"list-group-item list-group-item-action\">Mes candidatures</a>
        </div>
    ";
        } elseif ((twig_get_attribute($this->env, $this->source,         // line 16
($context["user"] ?? null), "company", [], "any", true, true) &&  !(null === twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 16, $this->source); })()), "company", [])))) {
            // line 17
            echo "        <span class=\"d-block\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 17, $this->source); })()), "company", []), "companyName", []), "html", null, true);
            echo "</span>
        <span class=\"d-block\">";
            // line 18
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 18, $this->source); })()), "company", []), "companyType", []), "html", null, true);
            echo "</span>
        ";
            // line 19
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 19, $this->source); })()), "request", []), "pathinfo", []) == "/")) {
                // line 20
                echo "        <a href=\"";
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profile_index");
                echo "\">Voir mon profil</a>
        ";
            } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 21
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 21, $this->source); })()), "request", []), "pathinfo", []) == "/profile")) {
                // line 22
                echo "        <a href=\"";
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profile_edit");
                echo "\">Modifier mon profil</a>
        ";
            }
            // line 24
            echo "        <div class=\"list-group\">
            <a href=\"#\" class=\"list-group-item list-group-item-action\">Offres publiées</a>
            <a href=\"#\" class=\"list-group-item list-group-item-action\">Candidatures reçues</a>
        </div>
    ";
        }
        // line 29
        echo "</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "block/profile.block.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  131 => 29,  124 => 24,  118 => 22,  116 => 21,  111 => 20,  109 => 19,  105 => 18,  100 => 17,  98 => 16,  92 => 12,  86 => 10,  84 => 9,  79 => 8,  77 => 7,  73 => 6,  66 => 5,  64 => 4,  60 => 2,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% block profile_block %}
<div class=\"col-3\">
    <img src=\"https://via.placeholder.com/150\">
    {% if user.candidate is defined and user.candidate is not null %}
        <span class=\"d-block\">{{ user.candidate.firstname }} {{ user.candidate.lastname }}</span>
        <span class=\"d-block\">{{ user.candidate.diploma }}</span>
        {% if app.request.pathinfo == '/' %}
        <a href=\"{{ path('profile_index') }}\">Voir mon profil</a>
        {% elseif app.request.pathinfo == '/profile' %}
        <a href=\"{{ path('profile_edit') }}\">Modifier mon profil</a>
        {% endif %}
        <div class=\"list-group\">
            <a href=\"#\" class=\"list-group-item list-group-item-action\">Mes offres favorites</a>
            <a href=\"#\" class=\"list-group-item list-group-item-action\">Mes candidatures</a>
        </div>
    {% elseif user.company is defined and user.company is not null %}
        <span class=\"d-block\">{{ user.company.companyName }}</span>
        <span class=\"d-block\">{{ user.company.companyType }}</span>
        {% if app.request.pathinfo == '/' %}
        <a href=\"{{ path('profile_index') }}\">Voir mon profil</a>
        {% elseif app.request.pathinfo == '/profile' %}
        <a href=\"{{ path('profile_edit') }}\">Modifier mon profil</a>
        {% endif %}
        <div class=\"list-group\">
            <a href=\"#\" class=\"list-group-item list-group-item-action\">Offres publiées</a>
            <a href=\"#\" class=\"list-group-item list-group-item-action\">Candidatures reçues</a>
        </div>
    {% endif %}
</div>
{% endblock %}", "block/profile.block.html.twig", "C:\\Symfony\\proxijob-project\\templates\\block\\profile.block.html.twig");
    }
}
